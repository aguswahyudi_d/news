package http

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"bitbucket.org/aguswahyudi_d/news/api"
	"bitbucket.org/aguswahyudi_d/news/api/http/news"
)

type (
	//Server will handle route and any services that exposed to user
	Server struct {
		server *http.Server
	}
)

func New(port string, newsService api.NewsService) *Server {
	s := new(Server)

	s.server = &http.Server{
		Addr:    port,
		Handler: getRouter(),
	}

	news.Init(newsService)

	return s
}

func (s *Server) Serve() {
	done := make(chan bool)
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT)
	signal.Notify(quit, syscall.SIGTERM)

	go func() {
		<-quit

		ctx, cancel := context.WithTimeout(context.Background(), 8*time.Second)
		defer cancel()

		// Keep Alive disabled when server is shutting down
		s.server.SetKeepAlivesEnabled(false)

		if err := s.server.Shutdown(ctx); err != nil {
			log.Printf("failed to shutting down gracefully: %v\n", err)
		}
		close(done)
	}()

	if err := s.server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Printf("can't listend on address: %s, err: %+v\n", s.server.Addr, err)
	}

	<-done
	fmt.Println("server stopped gracefully")
}
