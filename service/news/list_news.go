package news

import (
	"context"

	"github.com/pkg/errors"
)

type (
	ListNewsRequest struct {
		Page    int    `json:"page"`
		PerPage int    `json:"per_page"`
		Status  string `json:"status"`
		Topics  string `json:"topic"`
	}

	ListNewsResponse struct {
		News []DocNews `json:"news"`
	}
)

func (s *Service) ListNews(ctx context.Context, req *ListNewsRequest) (*ListNewsResponse, error) {
	from := req.Page - 1
	if from < 0 {
		from = 0
	}
	size := (from+1)*req.PerPage + 1

	req.Page = from
	req.PerPage = size

	res, err := s.repo.ListNews(ctx, req)
	if err != nil {
		return nil, errors.Wrap(err, "[service][ListNews] failed on fetch list news")
	}

	return res, nil
}
