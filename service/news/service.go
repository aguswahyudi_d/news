package news

import (
	"context"
)

type (
	// Service dependencies
	Service struct {
		repo repository
	}

	//DocNews mapping for Elasticsearch document `_source`
	DocNews struct {
		Title   string `json:"title"`
		Content string `json:"content"`
		Status  string `json:"status"`
		Topics  string `json:"topics"`
	}

	DocTopic struct {
		ID   string `json:"id"`
		Name string `json:"name"`
	}

	//repository is method from news repository that being used by news service
	repository interface {
		AddNews(ctx context.Context, req *DocNews) error
		AddTopic(ctx context.Context, topics []string) error
		ListTopic(ctx context.Context, from, size int) (*ListTopicResponse, error)
		ListNews(ctx context.Context, req *ListNewsRequest) (*ListNewsResponse, error)
	}
)

func New(repo repository) *Service {
	return &Service{
		repo: repo,
	}
}
