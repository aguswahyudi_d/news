package news

import (
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/aguswahyudi_d/news/api"
	"bitbucket.org/aguswahyudi_d/news/pkg/render"
	srvNews "bitbucket.org/aguswahyudi_d/news/service/news"
	"github.com/mholt/binding"
	"github.com/pkg/errors"
)

var (
	newsService api.NewsService
)

func Init(news api.NewsService) {
	newsService = news
}

func AddNews(w http.ResponseWriter, r *http.Request) {
	req := new(srvNews.AddNewsRequest)

	if err := binding.Bind(r, req); err != nil {
		log.Println("failed to bind: ", errors.Cause(err))
		render.WithError(r.Context(), render.HTTPError{
			Code:    http.StatusBadRequest,
			Error:   err,
			Message: "failed to bind parameter",
		})
	}

	err := newsService.AddNews(r.Context(), req)
	if err != nil {
		log.Println("failed to add news: ", errors.Cause(err))
		render.WithError(r.Context(), render.HTTPError{
			Code:    http.StatusInternalServerError,
			Error:   err,
			Message: "request failed",
		})
	}

	render.WithPayload(r.Context(), map[string]interface{}{
		"data": "success add news",
	})
}

func ListNews(w http.ResponseWriter, r *http.Request) {
	req := new(srvNews.ListNewsRequest)
	req.PerPage, _ = strconv.Atoi(r.URL.Query().Get("per_page"))
	req.Page, _ = strconv.Atoi(r.URL.Query().Get("page"))
	req.Status = r.URL.Query().Get("status")
	req.Topics = r.URL.Query().Get("topics")

	data, err := newsService.ListNews(r.Context(), req)
	if err != nil {
		log.Println("failed to list news: ", errors.Cause(err))
		render.WithError(r.Context(), render.HTTPError{
			Code:    http.StatusInternalServerError,
			Error:   err,
			Message: "request failed",
		})
	}

	render.WithPayload(r.Context(), data)
}
