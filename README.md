# news service

## Running the service

To run the service please make sure `docker` and `docker-compose` installed on the machine.
For Running the service run this command:
``` 
docker-compose up --build
```
or to run with specific `docker-compose` file:
``` 
docker-compose -f docker-compose.yml up --build 
```


## Development

For developing this service I am using this following setup:

Setup git message template
``` 
git config commit.template .gitmessage
```

Setup pre-commit
```
cp pre-commit .git/hooks/
chmod +x .git/hooks/pre-commit
```

# Constraint
- Topic name will containing no space character like hashtag

# API

## AddNews
Create a news 

- Method: POST
- Endpont: `news/add`

Request parameters:

|parameter|required|example|description|
|---|---|---|---|
|title|yes|Milenial 2019|Title of the news|
|content|no|Hello World|content of the news|
|status|no|0|status of the news (0: draft, 1: publish, 9: deleted, [default: 0])|
|topics|no|laptop,kamera|topics of the news. using comma separated value|

## AddTopic
Create a new topic.
Will remove any space on the given topic name

- Method: POST
- Endpont: `topic/add`

Request parameters:

|parameter|required|example|description|
|---|---|---|---|
|topic|yes|fashion|topic name that being inserted|

## ListTopic

Show list of all topics.

- Method: GET
- Endpont: `topic/add`

Request parameters:

|parameter|required|example|description|
|---|---|---|---|
|page|no|1|for pagination. default: 1|
|per_page|no|10|total item on a single page. default: 10|

## ListNews

Show list of all news. This API capable to do filtering by news status or topic

- Method: GET
- Endpont: `topic/add`

Request parameters:

|parameter|required|example|description|
|---|---|---|---|
|page|no|1|for pagination. default: 1|
|per_page|no|10|total item on a single page. default: 10|
|status|no|0|filtering status. (0: draft, 1: publish, 9: deleted|
|topics|no|fashion|topic filtering|