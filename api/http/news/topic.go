package news

import (
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/aguswahyudi_d/news/pkg/render"
	"bitbucket.org/aguswahyudi_d/news/service/news"
	"github.com/pkg/errors"
)

func AddTopic(w http.ResponseWriter, r *http.Request) {
	topic := r.FormValue("topic")

	err := newsService.AddTopic(r.Context(), topic)
	if err != nil {
		log.Println("failed to add news: ", errors.Cause(err))
		render.WithError(r.Context(), render.HTTPError{
			Code:    http.StatusInternalServerError,
			Error:   err,
			Message: "request failed",
		})
	}

	render.WithPayload(r.Context(), map[string]interface{}{
		"data": "success add topic",
	})
}

func ListTopic(w http.ResponseWriter, r *http.Request) {
	page, _ := strconv.Atoi(r.FormValue("page"))
	perPage, _ := strconv.Atoi(r.FormValue("per_page"))

	if page < 0 {
		page = 0
	}

	if perPage < 0 {
		perPage = 10
	}

	req := new(news.ListTopicRequest)
	req.PerPage = perPage
	req.Page = page

	data, err := newsService.ListTopic(r.Context(), req)
	if err != nil {
		log.Println("failed to list topic", errors.Cause(err))
		render.WithError(r.Context(), render.HTTPError{
			Code:    http.StatusInternalServerError,
			Error:   err,
			Message: "request failed",
		})
	}

	render.WithPayload(r.Context(), data)
}
