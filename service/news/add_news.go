package news

import (
	"context"
	"net/http"
	"strings"

	"github.com/pkg/errors"

	"github.com/mholt/binding"
)

type (
	// AddNewsRequest parameters
	AddNewsRequest struct {
		Title   string `json:"title"`
		Content string `json:"content"`
		Status  string `json:"status"`
		Topics  string `json:"topics"`
	}
)

const (
	ESIndexMedia = "media"
	ESTypeNews   = "news"
	ESTypeTopic  = "topic"

	StatusDraft   = "0"
	StatusPublish = "1"
	StatusDeleted = "9"
)

func (ar *AddNewsRequest) FieldMap(r *http.Request) binding.FieldMap {
	return binding.FieldMap{
		&ar.Title:   "title",
		&ar.Content: "content",
		&ar.Status:  "status",
		&ar.Topics:  "topics",
	}
}

// AddNews will insert data to elasticsearch.
// When the news has a topic that never been recorded, it will automatically add it to topic list.
func (s *Service) AddNews(ctx context.Context, req *AddNewsRequest) error {
	if req.Title == "" {
		return errors.New("empty title")
	}

	if req.Status == "" {
		req.Status = StatusDeleted
	}
	req.Topics = strings.TrimSpace(req.Topics)
	req.Topics = strings.ToLower(req.Topics)
	req.Topics = strings.Replace(req.Topics, " ", "_", -1)
	req.Topics = strings.Replace(req.Topics, ",", " ", -1)
	topics := strings.Split(req.Topics, " ")

	err := s.repo.AddTopic(ctx, topics)
	if err != nil {
		return errors.Wrap(err, "[service][AddNews] topic fail")
	}

	err = s.repo.AddNews(ctx, &DocNews{
		Title:   req.Title,
		Content: req.Content,
		Status:  req.Status,
		Topics:  req.Topics,
	})
	if err != nil {
		return errors.Wrap(err, "[service][AddNews] news fail")
	}

	return nil
}
