package news

import (
	elastic "gopkg.in/olivere/elastic.v5"
)

type (
	// Repository news dependency
	Repository struct {
		esClient *elastic.Client
	}
)

// New construct news repository
func New(esClient *elastic.Client) *Repository {
	return &Repository{
		esClient: esClient,
	}
}
