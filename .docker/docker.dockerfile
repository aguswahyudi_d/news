FROM golang:1.10 AS builder

RUN wget -O /usr/bin/dep https://github.com/golang/dep/releases/download/v0.5.0/dep-linux-amd64 && \
    chmod +x /usr/bin/dep

WORKDIR /go/src/bitbucket.org/aguswahyudi_d/news

COPY Gopkg.toml Gopkg.lock ./

RUN dep ensure -v -vendor-only

COPY . .

RUN make build

FROM alpine:latest

WORKDIR /root/
COPY --from=builder /go/src/bitbucket.org/aguswahyudi_d/news .

ENTRYPOINT ["./news"]