package render

import (
	"context"
	"encoding/json"
	"net/http"
)

type responseKey struct{}

type response struct {
	payload interface{}
}

func (r *response) write(payload interface{}) {
	r.payload = payload
}

func contextWithResponse(ctx context.Context) context.Context {
	return context.WithValue(ctx, responseKey{}, &response{})
}

func fromContext(ctx context.Context) (*response, bool) {
	r, ok := ctx.Value(responseKey{}).(*response)
	return r, ok
}

// WithPayload checking if context has correct value type to be rendered and write the given payload.
// Will panic if response JSON middleware wasn't used first
func WithPayload(ctx context.Context, payload interface{}) {
	r, ok := fromContext(ctx)
	if !ok {
		panic("should implement middleware for render JSON")
	}
	r.write(payload)
}

// WithError adds HTTPError to context response.
// Will panic if response JSON middleware wasn't used first
func WithError(ctx context.Context, err HTTPError) {
	WithPayload(ctx, err)
}

type HTTPError struct {
	// Code is http status code
	Code    int    `json:"status"`
	Error   error  `json:"-"`
	Message string `json:"error_message"`
}

func WithJSON(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var (
			b   []byte
			err error
		)

		// set http header
		w.Header().Set("Content-Type", "application/json")

		// create context with response holder
		ctx := contextWithResponse(r.Context())

		// call wrapped handler and pass request with new context
		next.ServeHTTP(w, r.WithContext(ctx))

		// get the response payload from context
		if response, ok := fromContext(ctx); ok {
			switch t := response.payload.(type) {
			case HTTPError:
				w.WriteHeader(t.Code)
			}

			if response.payload != nil {
				b, err = json.Marshal(response.payload)
				if err != nil {
					w.WriteHeader(http.StatusInternalServerError)
					b, err = json.Marshal(HTTPError{
						Code:    http.StatusInternalServerError,
						Error:   err,
						Message: http.StatusText(http.StatusInternalServerError),
					})
					if err != nil {
						panic("something wrong with marshal HTTP Error")
					}
				}
			}

			w.Write(b)
		}
	})
}
