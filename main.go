package main

import (
	"log"
	"time"

	httpapi "bitbucket.org/aguswahyudi_d/news/api/http"
	rpnews "bitbucket.org/aguswahyudi_d/news/repository/news"
	srvnews "bitbucket.org/aguswahyudi_d/news/service/news"
	"gopkg.in/olivere/elastic.v5"
)

func main() {
	var (
		err      error
		esClient *elastic.Client
	)

	esOpt := elastic.SetURL("http://elasticsearch:9200")

	retry := 5 // retry to connect to elasticsearch since docker elastic init take longer time then expected
	for retry > 0 {
		esClient, err = elastic.NewClient(esOpt)
		if err != nil {
			log.Print("failed to init elasticsearch: ", err)
		} else if err == nil {
			break
		}

		time.Sleep(3 * time.Second)

		retry--

		if retry == 0 {
			log.Fatal("stop retry to connect to elastic client")
		}
	}

	newsRepo := rpnews.New(esClient)
	newsService := srvnews.New(newsRepo)

	server := httpapi.New(":4040", newsService)
	server.Serve()
}
