package api

import (
	"context"

	newsSrv "bitbucket.org/aguswahyudi_d/news/service/news"
)

type NewsService interface {
	AddNews(ctx context.Context, req *newsSrv.AddNewsRequest) error
	AddTopic(ctx context.Context, topic string) error
	ListNews(ctx context.Context, req *newsSrv.ListNewsRequest) (*newsSrv.ListNewsResponse, error)
	ListTopic(ctx context.Context, req *newsSrv.ListTopicRequest) (*newsSrv.ListTopicResponse, error)
	//RemoveNews()
	//RemoveTopic()
	//UpdateNews()
	//UpdateTopic()
}
