#!/bin/bash

run:
	@echo "${NOW} == 🔧 BUILDING 🔧 =="
	@CGO_ENABLED=0 go build -o news main.go
	@echo "${NOW} == ▶️ RUNNING...️️ ▶=="
	@./news

build:
	@echo "${NOW} == 🔧 BUILDING 🔧 =="
	@CGO_ENABLED=0 go build -o news main.go

