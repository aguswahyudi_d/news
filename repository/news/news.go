package news

import (
	"context"
	"reflect"
	"strings"

	srvnews "bitbucket.org/aguswahyudi_d/news/service/news"
	"github.com/pkg/errors"
	"gopkg.in/olivere/elastic.v5"
)

// AddNews will POST to elasticsearch to insert news document.
// Will return error when failed to hit elasticsearch
func (r *Repository) AddNews(ctx context.Context, req *srvnews.DocNews) error {
	_, err := r.esClient.Index().
		Index(srvnews.ESIndexMedia).
		Type(srvnews.ESTypeNews).
		BodyJson(req).
		Do(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to hit elastic")
	}

	return nil
}

func (r *Repository) ListNews(ctx context.Context, req *srvnews.ListNewsRequest) (*srvnews.ListNewsResponse, error) {
	q := elastic.NewBoolQuery()

	if req.Status != "" {
		q = q.Must(elastic.NewTermQuery("status", req.Status))
	}

	if req.Topics != "" {
		req.Topics = strings.TrimSpace(req.Topics)
		req.Topics = strings.ToLower(req.Topics)
		req.Topics = strings.Replace(req.Topics, " ", "_", -1)
		req.Topics = strings.Replace(req.Topics, ",", " ", -1)
		topics := strings.Split(req.Topics, " ")

		tpc := make([]interface{}, len(topics))
		for i, t := range topics {
			tpc[i] = t
		}

		q = q.Must(elastic.NewTermsQuery("topics", tpc...))
	}

	res, err := r.esClient.Search().
		Index(srvnews.ESIndexMedia).
		Type(srvnews.ESTypeNews).
		Query(q).Size(req.PerPage).From(req.Page).Do(ctx)
	if err != nil {
		return nil, err
	}

	news := make([]srvnews.DocNews, 0, res.TotalHits())
	for _, item := range res.Each(reflect.TypeOf(srvnews.DocNews{})) {
		i := item.(srvnews.DocNews)
		news = append(news, i)
	}

	return &srvnews.ListNewsResponse{
		News: news,
	}, nil
}
