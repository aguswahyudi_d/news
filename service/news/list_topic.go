package news

import "context"

type (
	// ListTopicRequest parameter
	ListTopicRequest struct {
		Page    int `json:"page"`
		PerPage int `json:"per_page"`
	}

	// ListTopicResponse struct
	ListTopicResponse struct {
		Topics []DocTopic `json:"topics"`
	}
)

// ListTopic get topics from elasticsearch
func (s *Service) ListTopic(ctx context.Context, req *ListTopicRequest) (*ListTopicResponse, error) {
	from := req.Page - 1
	if from < 0 {
		from = 0
	}

	size := (from+1)*req.PerPage + 1

	res, err := s.repo.ListTopic(ctx, from, size)
	if err != nil {
		return nil, err
	}

	return res, nil
}
