package news

import (
	"context"
	"strings"

	"github.com/pkg/errors"
)

// AddTopic will insert topic to elasticsearch.
func (s *Service) AddTopic(ctx context.Context, topic string) error {
	topic = strings.TrimSpace(topic)
	topic = strings.ToLower(topic)
	topic = strings.Replace(topic, " ", "_", -1)
	topic = strings.Replace(topic, ",", " ", -1)
	err := s.repo.AddTopic(ctx, []string{topic})
	if err != nil {
		return errors.Wrap(err, "[service][AddTopic] add topic failed")
	}

	return nil
}
