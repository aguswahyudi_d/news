package http

import (
	"bitbucket.org/aguswahyudi_d/news/api/http/news"
	"bitbucket.org/aguswahyudi_d/news/pkg/render"
	"github.com/go-chi/chi"
)

func getRouter() *chi.Mux {
	r := chi.NewRouter()

	r.Group(func(r chi.Router) {
		r.Use(render.WithJSON)

		r.Post("/news/add", news.AddNews)
		r.Get("/news/list", news.ListNews)

		r.Post("/topic/add", news.AddTopic)
		r.Get("/topic/list", news.ListTopic)

	})

	return r
}
