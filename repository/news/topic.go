package news

import (
	"context"
	"reflect"

	srvnews "bitbucket.org/aguswahyudi_d/news/service/news"
	"github.com/pkg/errors"
	"gopkg.in/olivere/elastic.v5"
)

// AddTopic will add new topic if the topic never been added before.
// Will return error on syntax error or request error
func (r *Repository) AddTopic(ctx context.Context, topics []string) error {
	bulkRequest := r.esClient.Bulk().Index(srvnews.ESIndexMedia).Type(srvnews.ESTypeTopic)

	for _, topic := range topics {
		req := elastic.NewBulkUpdateRequest().Index(srvnews.ESIndexMedia).Type(srvnews.ESTypeTopic).Id(topic).
			Doc(srvnews.DocTopic{
				ID:   topic,
				Name: topic,
			}).DocAsUpsert(true)
		bulkRequest.Add(req)
	}

	bulkResp, err := bulkRequest.Do(ctx)
	if err != nil {
		return errors.Wrap(err, "failed to bulk add topic")
	} else if bulkResp.Errors {
		return errors.Wrap(err, "wrong syntax on bulk add topic")
	}

	return nil
}

// ListTopic using search API from elastic to retrieve all topic based on from and size
func (r *Repository) ListTopic(ctx context.Context, from, size int) (*srvnews.ListTopicResponse, error) {
	res, err := r.esClient.Search().Index(srvnews.ESIndexMedia).Type(srvnews.ESTypeTopic).Size(size).From(from).Do(ctx)
	if err != nil {
		return nil, err
	}

	topics := make([]srvnews.DocTopic, 0, res.TotalHits())
	for _, item := range res.Each(reflect.TypeOf(srvnews.DocTopic{})) {
		i := item.(srvnews.DocTopic)
		topics = append(topics, i)
	}

	return &srvnews.ListTopicResponse{
		Topics: topics,
	}, nil

}
