package render

import (
	"context"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestWithPayload(t *testing.T) {
	t.Run("panic on unexpected payload type", func(t *testing.T) {
		ctx := context.WithValue(context.Background(), responseKey{}, "middleware is not initialized")

		assert.Panics(t, func() {
			WithPayload(ctx, "something")
		})
	})

	t.Run("write payload to responseKey value when payload type is correct", func(t *testing.T) {
		assert.NotPanics(t, func() {
			ctx := contextWithResponse(context.Background())
			want := "this is data"

			WithPayload(ctx, want)

			got, ok := ctx.Value(responseKey{}).(*response)
			assert.True(t, ok)
			assert.Equal(t, want, got.payload)
		})
	})
}

func TestWithError(t *testing.T) {
	t.Run("panic on unexpected payload type", func(t *testing.T) {
		ctx := context.WithValue(context.Background(), responseKey{}, "middleware is not initialized")

		assert.Panics(t, func() {
			WithError(ctx, HTTPError{})
		})
	})

	t.Run("write payload to responseKey value when payload type is HTTPError", func(t *testing.T) {
		assert.NotPanics(t, func() {
			ctx := contextWithResponse(context.Background())

			want := HTTPError{
				Code:    500,
				Message: "this is error",
			}
			WithError(ctx, want)

			got, ok := ctx.Value(responseKey{}).(*response)
			assert.True(t, ok)
			assert.Equal(t, want, got.payload)
		})
	})
}

func TestWithJSON(t *testing.T) {
	type testCase struct {
		mock     func() http.HandlerFunc
		expected []byte
	}

	cases := make(map[string]testCase)
	cases["middleware can write data with payload to JSON"] = testCase{
		mock: func() http.HandlerFunc {
			fn := func(w http.ResponseWriter, r *http.Request) {
				WithPayload(r.Context(), map[string]interface{}{
					"data": "hello",
				})
			}
			return http.HandlerFunc(fn)
		},
		expected: []byte(`{"data":"hello"}`),
	}

	cases["middleware can write HTTPError to JSON"] = testCase{
		mock: func() http.HandlerFunc {
			fn := func(w http.ResponseWriter, r *http.Request) {
				WithError(r.Context(), HTTPError{
					Message: "this is an error",
					Code:    http.StatusInternalServerError,
				})
			}
			return http.HandlerFunc(fn)
		},
		expected: []byte(`{"status":500,"error_message":"this is an error"}`),
	}

	cases["middleware receive unsupported type to Marshal"] = testCase{
		mock: func() http.HandlerFunc {
			fn := func(w http.ResponseWriter, r *http.Request) {
				WithPayload(r.Context(), make(chan bool))
			}
			return http.HandlerFunc(fn)
		},
		expected: []byte(`{"status":500,"error_message":"Internal Server Error"}`),
	}

	for name, tt := range cases {
		t.Run(name, func(t *testing.T) {
			ts := httptest.NewServer(WithJSON(tt.mock()))
			defer ts.Close()

			res, err := http.Get(ts.URL)
			assert.NoError(t, err)

			got, err := ioutil.ReadAll(res.Body)
			res.Body.Close()
			assert.NoError(t, err)

			assert.Equal(t, tt.expected, got)
			assert.Equal(t, "application/json", res.Header.Get("Content-Type"))
		})
	}

}
